$(document).ready(function () {
 
    var $body = $("body"),
		$wrapper = $("#wrapper"),
		$menu = $('#menu'),
		$loader = $('#loader'),
		tracksurf = null;


	TS.debug = QueryString.dbg == 1 || QueryString.dbg == 'true';

	// init loader
	TS.Loader
		.init($loader)
		.set(0);


	// init menu
	TS.Menu
		.init($menu, function (level) {
			
			// start game
			TS.Menu.hide(undefined, startGame.bind(this, level));

		}, function (level) { // retry game

			tracksurf.models.track.retryGame();
			TS.Menu.hide(undefined, startGame.bind(this, level));

		}, function () { // go back to main menu
			
			tracksurf.models.track.resetGame();
			TS.Menu.menu('startmenu');

		})
		.hide(0);


	// make sure all fonts are available
	TS.FontLoader.loadFonts($wrapper, 'TS', 'TSB', 'TSM');

	function startGame(level) {
		if(! TS.debug)
			tracksurf.setCamera('track');

		tracksurf
			.rotateTrack(false)
			.setTrackDifficultyLevel(level)
			.startGame(onGameEnd);

		$wrapper.addClass('hidecursor');

	}

	function prepareGame() {

		var width = $wrapper.width(),
			height = $wrapper.height();

		tracksurf = new TS.TrackSurf({
			container: $wrapper.get(0),
			width: width,
			height: height,
			stats: TS.debug,
			countDownSecs: 3,
			track: {
				trackTime: 3,
				texture: 'assets/texture/strips.png',
				scale: 1,
				trackCamAnimOffsetX: 180,
				trackCamAnimOffsetY: 50,
				trackCamAnimOffsetZ: 0,
				trackCamAnimDuration: 7000,
				shipStartOffset: .002,
				packetsExplodable: 22,
				packetsFire: 35,
				packetsBad: 35,
				firstPacketOffset: 4,
				lastPacketOffset: 3
			},
			ship: {
				texture: 'js/tracksurf/model/models/dark_fighter/dark_fighter.js',
				engineFireTexture: 'assets/texture/smoke.png',
				engineFireColor: 0xff0000,
				scale: .23,
				trackCnt: 3,
				steerTime: 250,
				steeringMaxAngle: 15,
				steerSensibility: .2
			},
			packet: {
				texture: 'js/tracksurf/model/models/packet/packet.js',
				explodeTexture: 'assets/texture/circle.png',
				opacity: .9,
				emissive: 0x00aa00,
				ambient: 0xff0000,
				color: 0x00aa00,
				specular: 0xff0000,
				shinines: 20,
				scale: .9,
				packetExplodeBonus: 15,
				packetFireBonus: 10,
				packetBadBonus: -5,
				soundExplode: 'assets/sound/explode.mp3',
				soundBurn: 'assets/sound/burn.mp3',
				soundBad: 'assets/sound/oou.mp3',

			},
			HUD: {
				width: width,
				height: height,
				container: $wrapper.get(0),
				countDownFont: 'TSM',
				countdownSize: 200,
				coutndounwRGB: [74, 91, 77],
				pointsFont: 'TSB',
				pointsSize: 80,
				pointsRGB: [74, 91, 77],
				pointAnimationTime: 250,
				goWord: 'GO'
			}
		}, function () {
			
			setupDatGUI();

			tracksurf.run();

			// debug
			if(TS.debug) {
				new THREE.OrbitControls(tracksurf.getCamera('main'));

				dbgRestore();

			} else {

				tracksurf.setCamera('main');

			}

			TS.Loader.hide(
				undefined, 
				TS.Menu.show.bind(
					TS.Menu, undefined, tracksurf.rotateTrack.bind(tracksurf, true)
				)
			);


		}, function (percent) {
			TS.Loader.set(percent);
		});

	}	


	function onGameEnd(data) {
		
		$wrapper.removeClass('hidecursor');

		TS.Menu
			.menu('retrymenu')
			.show()
			.animateScore(data.points, data.availablePoints);

		if(! TS.debug)
			tracksurf.setCamera('main');

		tracksurf.rotateTrack(true);


	}

	// start the game delayed
	setTimeout(prepareGame, 50);




	// DEBUGGING
	function setupDatGUI() {
		if(! TS.debug)
			return;

		var settings = {
			camchange: function() {
				var cam = tracksurf.camera.name,
					cams = Object.keys(tracksurf.cameras),
					camPos = cams.indexOf(cam),
					next = (camPos + 1) % cams.length;
				tracksurf.camera = tracksurf.cameras[cams[next]];
				dbgSet('cam', cams[next]);
			},
			togglemenu: function() {
				$menu.toggle();
			}
		};

		var track = tracksurf.getModel('track');
		var gui = new dat.GUI({css: {zIndex: 5}});

		gui.add(track, 'trackTime', 1, 1500);
		gui.add(settings, 'camchange');
		gui.add(settings, 'togglemenu');

		gui.add(ts, "_rotateTrack").listen();

		$('.dg.ac').on('click mousedown', function (e) {			
			e.preventDefault();
			return false;
		});

	}

	function dbgRestore() {
		// get stored cam
		var cam = dbgGet('cam');
		if(cam)
			tracksurf.setCamera(cam);
	}

	function dbgSet(key, val) {
		if(! localStorage) {
			console.error('localStorage not available.');
			return;
		}
		localStorage.setItem(key, val);
	}

	function dbgGet(key) {
		if(! localStorage) {
			console.error('localStorage not available.');
			return;
		}
		return localStorage.getItem(key);
	}

});



