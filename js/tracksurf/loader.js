

var TS = TS || {};

(function () {
	
	var disabled = false,
		$loader = null,
		$percent = null,
		percent = null,
		$leContainer = null;


	var interface = {

		init: function($container, onStart) {
			$leContainer = $container;
			var $divs = $container.find('>div');
			$loader = $divs.eq(0);
			$percent = $divs.eq(1);
			return interface;
		},

		show: function(time, callback) {
			$leContainer.fadeIn(time !== undefined ? time : 'fast', callback);
			return interface;
		},

		hide: function(time, callback) {
			$leContainer.fadeOut(time !== undefined ? time : 'fast', callback);
			return interface;
		},

		set: function(_percent) {
			percent = Math.max(0, Math.min(100, Math.round(_percent)));
			$loader.width(percent + '%');
			setTimeout(function () {
				$percent.html(percent + '%');
			}, 250);
			return interface;
		}
		
	};

	TS.Loader = interface;

})();