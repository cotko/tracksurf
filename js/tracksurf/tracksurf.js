
var TS = TS || {};

(function () {

	var mparent = TS.GameLoop,
		parent = mparent.prototype;

	var percentLoaded = 0,
		percentStep = 100 / 6;

	TS.TrackSurf = function(conf) {
		mparent.apply(this, arguments);
		if(TS.debug)
			window.ts = this;

		this.userData.packetExplodeBonus = this.conf.packet.packetExplodeBonus || 8;
		this.userData.packetBadBonus = this.conf.packet.packetBadBonus || -525;
		this.userData.packetFireBonus = this.conf.packet.packetFireBonus || 5;

		this._rotateTrack = false;
	};

	TS.TrackSurf.prototype = Object.create(parent);

	var m = TS.TrackSurf,
		p = m.prototype;

	p.startGame = function(cbEnd, trackTime) {
		this.HUD().setPoints(0);

		var track = this.getModel('track');

		if(trackTime !== undefined)
			track.trackTime = trackTime;

		this.userData.points = 0;

		track.start(this.conf.countDownSecs || 5, function () {

			track.paused(true);

			this.HUD().clearPoints();

			this.userData.availablePoints = this.calculateAvailablePoints();

			if(cbEnd) cbEnd(this.userData);

		}.bind(this));

		return this;

	};

	p.setTrackDifficultyLevel = function(level) {
		level = Math.min(4, Math.max(1, level));
		var track = this.getModel('track').setTrackTime(this.difficultyLevel2trackTime(level));
		this.userData.level = level;

		var shipSteeringAngle = this.conf.ship.steeringMaxAngle,
			steerSensibility = this.conf.ship.steerSensibility;
		switch(level) {
		case 3:
			shipSteeringAngle += 15;
			steerSensibility *= 1.3;
			break;
		case 4:
			shipSteeringAngle += 20;
			steerSensibility *= 1.5;
			break;
		}

		track.ship
				.maxSteeringAngle(shipSteeringAngle)
				.steerSensibility(steerSensibility);


		return this;
	};

	p.difficultyLevel2trackTime = function(level) {
		var multiplier = 0.0536; // normal mode

		switch(level) {
		case 1: // granny
			multiplier = 0.0827;
			break;
		case 3:
			multiplier = 0.0441;
			break;
		case 4:
			multiplier = 0.0291;
			break;
		}

		return this.getModel('track').tube.path.getLength() * multiplier;
	};



	p.initScene = function() {
		parent.initScene.apply(this, arguments);

		var camera = this.camera,
			scene = this.scene;

		var ambient = 0x74CC6F, diffuse = 0x4d4d4d, specular = 0xffffff, shininess = 42, scale = 23;

		scene.add(new THREE.AmbientLight(ambient));

		//SUN
		// var sun = new THREE.DirectionalLight(diffuse, 100.5);
		// sun.position.set(0, 3, 2);
		// sun.lookAt(new THREE.Vector3());

		
		// sun.castShadow = true;
		// sun.shadowCameraNear = 50;
		// sun.shadowCameraFar = camera.far*2;
		// sun.shadowCameraRight     =  100;
		// sun.shadowCameraLeft      = -100;
		// sun.shadowCameraTop       =  100;
		// sun.shadowCameraBottom    = -100;
		// sun.shadowCameraVisible = true;
		// sun.shadowBias = 0.0001;
		// sun.shadowDarkness = 0.7;
		// sun.shadowMapWidth = 48;
		// sun.shadowMapHeight = 48;
		
		// scene.add(sun);

		var sun2 = new THREE.DirectionalLight(diffuse, 1.5);
		sun2.position.set(200, -300, -200);
		sun2.lookAt(new THREE.Vector3(20,20,5));

		
		// sun2.castShadow = true;
		// sun2.shadowCameraNear = 50;
		// sun2.shadowCameraFar = camera.far*2;
		// sun2.shadowCameraRight     =  100;
		// sun2.shadowCameraLeft      = -100;
		// sun2.shadowCameraTop       =  100;
		// sun2.shadowCameraBottom    = -100;
		// sun2.shadowCameraVisible = true;
		// sun2.shadowBias = 0.0001;
		// sun2.shadowDarkness = 0.7;
		// sun2.shadowMapWidth = 48;
		// sun2.shadowMapHeight = 48;
		
//		scene.add(sun2);

		var hemiLight = new THREE.HemisphereLight( 0xffffff, 0xffffff, 1 );
				hemiLight.color.setHSL( 0.6, 1, 0.6 );
				hemiLight.groundColor.setHSL( 0.095, 1, 0.75 );
				hemiLight.position.set( 1, 1,1 );
				//scene.add( hemiLight );

		// var light   = new THREE.SpotLight( 0xffffff, 1, 20 );
		// light.position.set( 0, 100, 0 );
		// light.rotation.set( 0, Math.PI, 0 );
		// light.shadowCameraFar = 50;
		// light.shadowCameraNear      = 0.01;     
		// light.castShadow        = true;
		// light.shadowDarkness        = 0.5;
		// light.shadowCameraVisible   = true;
		// light.shadowCameraFar = 800;
		// light.shadowCameraFov = 15;
		// scene.add( light );

		this.notifyLoaded();

		
		return this;
	};

	p.initModels = function(callback) {

  		var self = this,
  			done = _.after(2, callback);

  		var prepareShip = function() {
  			track.prepare(function () {
  				self.notifyLoaded();
				done();
  			});
  		};

  		var track = new TS.models.Track(this, this.conf.track).load(null, this.scene, new THREE.Vector3(1,1,-30), function () {

  			self.notifyLoaded();

  			var loaded = _.after(2, prepareShip);

			track
				.loadShip(self.conf.ship, function () {
					self.notifyLoaded();
					loaded();
				})
				.loadPackets(self.conf.packet, function () {
					self.notifyLoaded();
					loaded();	
				});

		});

		track.onCollision = function(packetData) {
			
			self.playCollisionSound(packetData)
				.addBonus(packetData);

		};

		this.initCollisionSounds(done);

	
		this.addModel(track, 'track');
		
	// 	var light = new THREE.PointLight(0xffffff);
	// light.position.set(100,250,100);
	// this.scene.add(light);

		//this.test();
		
		
		return this;
	};

	p.initCollisionSounds = function(callback) {

		var self = this,
			done = _.after(3, function () {

				self.notifyLoaded();
				
				callback();

			});

		TS.media.AudioLoader(this.conf.packet.soundExplode, function (audio) {
			self.soundExplode = audio;
			done();
		});

		TS.media.AudioLoader(this.conf.packet.soundBurn, function (audio) {
			self.soundBurn = audio;
			done();
		});

		TS.media.AudioLoader(this.conf.packet.soundBad, function (audio) {
			self.soundBad = audio;
			done();
		});

		return this;
	};

	p.rotateTrack = function(rotate) {
		if(rotate !== undefined) {
			this._rotateTrack = !! rotate;
			return this;
		}
		
		return this._rotateTrack;

	};

	p.playCollisionSound = function(packetData) {
		var sound = null;

		switch(packetData.type) {
		case 'explodable': 
			sound = this.soundExplode;
			break;
		case 'fire': 
			sound = this.soundBurn;
			break;
		case 'bad': 
			sound = this.soundBad;
			break;
		}
		
		sound.pause();
		sound.currentTime = 0;
		sound.play();
		return this;
	};

	p.addBonus = function(packetData) {

		var points = this.userData.points || 0,
			bonus = this.packetType2points(packetData.type);

		this.HUD().addPoints(points, bonus);
		this.userData.points = points + bonus;

		return this;
	};

	p.packetType2points = function(type) {
		var bonus = this.userData.level - 2;

		switch(type) {
		case 'explodable': 
			bonus += this.userData.packetExplodeBonus;
			break;
		case 'fire': 
			bonus += this.userData.packetFireBonus;
			break;
		case 'bad': 
			bonus += this.userData.packetBadBonus;
			break;
		}
		return bonus;
	};

	p.calculateAvailablePoints = function() {
		return  (this.packetType2points('explodable') * (this.conf.track.packetsExplodable || 0)) +
				(this.packetType2points('fire') * (this.conf.track.packetsFire || 0));
				//(this.packetType2points('bad') * (this.conf.track.packetsBad || 0)); // bad go into negative
	};

	p.update = function(now, dt) {
		parent.update.apply(this, arguments);

		var track = this.getModel('track');
		
		
		if(track && this.rotateTrack())
			this.updateRotatingTrack(now, dt, track);

if(! window.uniforms)
	return this;
var time = Date.now() * 0.001;
var interval = 10;
window.uniforms.amplitude.value = 0.125 * Math.sin( time * 0.5 );
window.uniforms.time.value = time % interval / interval; //1.25 * Math.sin( time * 0.5 );
// window.uniforms.time.value = 1.25 * Math.sin( time * 0.5 );


		return this;
	};

	p.handleKeyboard = function() {

		var leftDown = TS.KeyboardHandler.isDown(TS.KeyboardHandler.keyCode.LEFT),
			rightDown = TS.KeyboardHandler.isDown(TS.KeyboardHandler.keyCode.RIGHT),
			ship = this.getModel('track').ship;

		if(rightDown && leftDown) {

			ship.unsteer();

		} else if(leftDown) {
			
			ship.steerLeft();

		} else if(rightDown) {
			
			ship.steerRight();

		} else {

			ship.unsteer();

		}
		
		return parent.handleKeyboard.apply(this, arguments);
	};


	var rotateSceneAxis = new THREE.Vector3(1, 1, 0).normalize(),
		rotObjectMatrix = new THREE.Matrix4();

	p.updateRotatingTrack = function(now, dt, track) {

		if(! this.scene)
			return this;

		rotObjectMatrix
			.identity()
			.makeRotationAxis(rotateSceneAxis, THREE.Math.degToRad(dt * 15))
			.multiply(track.mesh.matrix);

		track.mesh.matrix = rotObjectMatrix;
    	track.mesh.rotation.setFromRotationMatrix(track.mesh.matrix);

		return this;
	};

	p.notifyLoaded = function() {
		percentLoaded += percentStep;
		this.onLoad(percentLoaded);
		return this;
	};

	p.test = function() {

		var self = this;

		TS.model.Loader.load('js/tracksurf/model/models/packet/packet.js', function (geometry, mats) {

			
			
			// var paket = track.packets[1];
			// 	paket.scene.remove(paket);
			// 	self.scene.add(paket);
			// 	paket.mesh.position.x = 0;
			// 	paket.mesh.position.y = 0;
			// 	paket.mesh.position.z = 37;
			// 	console.log(paket)
			


			geometry.dynamic = true;

			THREE.GeometryUtils.center( geometry );

			var i, n = 2, maxEdgeLength = .1;
			var em = new THREE.ExplodeModifier();
			var tm = new THREE.TessellateModifier(maxEdgeLength);

			for ( i = 0; i < n; i ++ ) 
				tm.modify( geometry );

			em.modify(geometry);	

			

        	window.attributes = {

                    displacement: {        type: 'v3', value: [] },
                    customColor:  {        type: 'c', value: [] }

            };

            window.uniforms = {
            		time: { type: "f", value: 0.0 },
                    amplitude: { type: "f", value: 0.0 }

            };

            var shaderMaterial = new THREE.ShaderMaterial( {

                    uniforms:                 uniforms,
                    attributes:     attributes,
                    vertexShader:   document.getElementById( 'packet_explode_vertexshader' ).textContent,
                    fragmentShader: document.getElementById( 'packet_explode_fragmentshader' ).textContent,
                    shading:                 THREE.FlatShading,
                    side:                         THREE.DoubleSide,
                    opacity: .9,
                    transparent: true

            });

            window.shaderMaterial = shaderMaterial;

			var vertices = geometry.vertices;

            var colors = attributes.customColor.value;
            var displacement = attributes.displacement.value;

            var nv, v = 0;

            for ( var f = 0; f < geometry.faces.length; f ++ ) {

                var face = geometry.faces[ f ];

                if ( face instanceof THREE.Face3 ) {

                        nv = 3;

                } else {

                        nv = 4;

                }

                var h = 0.15 * Math.random();
                var s = 0.5 + 0.5 * Math.random();
                var l = 0.5 + 0.5 * Math.random();

                var d = 10 * (  Math.random() );

                var x = 2 * ( 0.5 - Math.random() );
                var y = 2 * ( 0.5 - Math.random() );
                var z = 2 * ( .5 - Math.random() );

                for ( var i = 0; i < nv; i ++ ) {

                        colors[ v ] = new THREE.Color();
                        displacement[ v ] = new THREE.Vector3();

                        //colors[ v ].setHSL( h, s, l );
                        colors[ v ].set( 0x00aa00 );
                        colors[ v ].convertGammaToLinear();

                        displacement[ v ].set( x + f%3 + d, y +f%2 , z + f%2 );

                        v += 1;

                }

            }


			
			
			paket = new THREE.Mesh( geometry, shaderMaterial );
                        paket.rotation.set( 0.5, 0.5, 0 );

                        self.scene.add( paket );

			paket.position.x = 0;
			paket.position.y = 0;
			paket.position.z = 5;
		

		});



	};

})();




