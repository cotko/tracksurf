

var TS = TS || {};

(function () {
	
	var disabled = false,
		$leContainer = null,
		$points = null,
		$allPoints = null;


	var interface = {

		init: function($container, onStart, onRetry, onMainMenu) {
			$leContainer = $container;
			$container
				.find('.levels > div')
					.on('click', function () {
						
						if(disabled)
							return;

						$(this)
							.siblings()
								.removeClass('selected', false)
								.end()
							.addClass('selected');
					})
					.end()
				.find('#startbutton')
					.on('click', function () {
						
						if(disabled)
							return;
						
						onStart($container.find('.levels .selected').index() + 1);
					
					})
					.end()
				.find('#retrybutton')
					.on('click', function () {
						
						if(disabled)
							return;
						
						onRetry($container.find('.levels .selected').index() + 1);
					
					})
					.end()
				.find('#mainmenubutton')
					.on('click', function () {
						
						if(disabled)
							return;
						
						onMainMenu($container.find('.levels .selected').index() + 1);
					
					});

					

			$points = $container.find('#myscore');
			$allPoints = $container.find('#allscore');

			return interface;
		},

		menu: function(which) {
			$leContainer.removeClass('retrymenu startmenu').addClass(which);
			switch(which) {
			case 'retrymenu':
				$points.html('');
				$allPoints.html('');
				break;
			}
			return interface;
		},

		show: function(time, callback) {
			this.enable();
			$leContainer.fadeIn(time !== undefined ? time : 'fast', callback);
			return interface;
		},

		hide: function(time, callback) {
			this.disable();
			$leContainer.fadeOut(time !== undefined ? time : 'fast', callback);
			return interface;
		},

		disable: function() {
			disabled = true;
			$leContainer.addClass('disabled');
			return interface;
		},

		enable: function() {
			disabled = false;
			$leContainer.removeClass('disabled');
			return interface;
		},

		animateScore: function(points, allpoints) {
			function animatePoints() {
				$({p: 0}).animate({p: points}, {
					step: function(val) {
						$points.html(Math.round(val));
					},
					complete: animateAllPoints
				});
			}

			function animateAllPoints() {
				$({p: 0}).animate({p: allpoints}, {
					step: function(val) {
						$allPoints.html(Math.round(val));
					}
				});
			}

			animatePoints();
			
			return interface;
		}
		
	};

	TS.Menu = interface;

})();