
var TS = TS || {};

(function() {

	TS.PacketFactory = function() {

	};

	var m = TS.PacketFactory,
		p = m.prototype,
		cachedTextures = {};

	m.getTexture = function(path, callback) {
		if(! cachedTextures[path]) {
			TS.model.Loader.load(path, function (geometry, mats) {
				cachedTextures[path] = [geometry, mats];
				callback(geometry, mats);
			});
		} else {
			callback.apply(this, cachedTextures[path]);
		}
		return this;
	};

	m.instantiateFirePackets = function(gameLoop, conf, howMany, callback) {
		
		var packets = [],
			done = _.after(howMany + 1, function () {
				callback(packets);
			}),
			pconf = $.extend(true, {}, conf);

		if(! pconf.collisionEffect)
			pconf.collisionEffect = 'fire';
			
		this.getTexture(conf.texture, function (geometry, mats) {
			
			for(var ii = 0; ii < howMany; ii++) {
				
				var pmats = [];
				for(var mm in mats) pmats.push(mats[mm].clone());

				var packet = new TS.models.Packet(gameLoop, pconf);
				packets.push(packet);
				packet._oncreated(pmats, null, null, geometry);
				packet._onload(done);
			}

		});
		
		done();

		return this;
	};

	m.instantiateBadPackets = function(gameLoop, conf, howMany, callback) {
		var pconf = $.extend(true, {}, conf);

		pconf.emissive = 0x1D1D1D;
		pconf.ambient = 0x111111;
		pconf.specular = 0xdd0000;
		pconf.color = 0x000000;
		pconf.shinines = .1;
		
		pconf.collisionEffect = 'bad';

		return this.instantiateFirePackets(gameLoop, pconf, howMany, callback);
	};

	m.instantiateExplodePackets = function(gameLoop, conf, howMany, callback) {
		
		var pconf = $.extend(true, {}, conf),
			vertexshader = document.getElementById('packet_explode_vertexshader').textContent,
			fragmentshader = document.getElementById('packet_explode_fragmentshader').textContent;
		
		pconf.vertexshader = vertexshader;
		pconf.fragmentshader = fragmentshader;
		pconf.collisionEffect = 'explode';

		return this.instantiateFirePackets(gameLoop, pconf, howMany, callback);
	};

})();