
var TS = TS || {};

if(! TS.models) TS.models = {};

(function() {

	var mparent = TS.model,
		parent = mparent.prototype;

	TS.models.Packet = function(gameLoop, conf) {
		mparent.apply(this, arguments);
		this._explodeAttributes = {};
		this._explodeUniforms = {};
	};


	TS.models.Packet.prototype = Object.create(parent);

	var m = TS.models.Packet,
		p = m.prototype;

	m.explodeTextures = {};

	p.load = function(path, scene, position, callback) {
		var self = this;
		return parent.load.call(this, path, scene, position, function () {
			self._onload(callback);
		});
	};
	
	p._onload = function(callback) {
		this.mesh.up = new THREE.Vector3(0, 1, 0);
		
		this.calculateCenter();

		this.updateMaterial();

		if(TS.debug)
			this._showHelpers();
		
		this.setCollisionEffect(callback);
		return this;
	};

	p.setCollisionEffect = function(callback) {
		console.log('this.conf.collisionEffect', this.conf.collisionEffect)
		switch(this.conf.collisionEffect) {
		case 'explode':
			callback();
			break;
		case 'fire':
			this._addExplodeFireEffect(callback);
			break;
		default:
			callback();
			break;
		}
		return this;
	};

	p.updateMaterial = function() {
		var material = this.mesh.material && this.mesh.material.materials instanceof Array ? this.mesh.material.materials[0] : this.mesh.material;
		material.transparent = true;
		material.opacity = this.conf.opacity || 1;
		if(material.emissive !== undefined && this.conf.emissive !== undefined)
			material.emissive.set(this.conf.emissive);
		if(material.color !== undefined && this.conf.color !== undefined)
			material.color.set(this.conf.color);
		if(material.specular !== undefined && this.conf.specular !== undefined)
			material.specular.set(this.conf.specular);
		if(material.ambient !== undefined && this.conf.ambient !== undefined)
			material.ambient.set(this.conf.ambient);
		if(material.shinines !== undefined && this.conf.shinines !== undefined)
			material.shinines = this.conf.shinines;
		return this;
	};

	p._createMesh = function(mats, geometry) {
		switch(this.conf.collisionEffect) {
		case 'explode':
			return this._getExplodableMesh(mats, geometry);
			break;
		case 'fire':
		default:
			return parent._createMesh.apply(this, arguments);
			break;
		}
		return this;
	};

	p._getExplodableMesh = function(mats, geometry) {
		
		if(! geometry._explodeprepared) {
			geometry.dynamic = true;
			THREE.GeometryUtils.center(geometry);

			// tessellate the geometry
			var n = 5, 
				maxEdgeLength = .001;
				em = new THREE.ExplodeModifier(),
				tm = new THREE.TessellateModifier(maxEdgeLength);

			for(var i = 0; i < n; i++) 
				tm.modify(geometry);

			// create "standalone" triangles from the geometry
			em.modify(geometry);

			this._explodeAttributes = {
				displacement: {type: 'v3', value: []},
				customColor: {type: 'c', value: []}
			};

			this._explodeUniforms = {
				time: {type: 'f', value: 0}
			};

			var vertices = geometry.vertices,
				colors = this._explodeAttributes.customColor.value,
				displacement = this._explodeAttributes.displacement.value;

            var nv, v = 0;

            for ( var f = 0; f < geometry.faces.length; f ++ ) {

                var face = geometry.faces[ f ];

                if ( face instanceof THREE.Face3 ) {

                        nv = 3;

                } else {

                        nv = 4;

                }

                var h = 0.15 * Math.random();
                var s = 0.5 + 0.5 * Math.random();
                var l = 0.5 + 0.5 * Math.random();

                var d = Math.random() * 2 - 1;

                var x = 2 * ( 0.5 - Math.random() );
                var y = 2 * ( 0.5 - Math.random() );
                var z = 2 * ( .5 - Math.random() );

                for ( var i = 0; i < nv; i ++ ) {

                        colors[ v ] = new THREE.Color();
                        displacement[ v ] = new THREE.Vector3();

                        //colors[ v ].setHSL( h, s, l );
                        colors[ v ].set( this.conf.emissive );
                        colors[ v ].convertGammaToLinear();

                        displacement[ v ].set( x + d, y + 2 , z - 2 );

                        v += 1;

                }

            }
            geometry._explodeprepared = {
            	explodeAttributes: {
					displacement: {type: 'v3', value: _.extend([], displacement)},
					customColor: {type: 'c', value: _.extend([], colors)}
				},
				explodeUniforms: {
					time: {type: 'f', value: 0}
				},
				em: em
            };
        } else {
        	//geom._explodeprepared.em.modify(geometry);
        	this._explodeAttributes = {
				displacement: {type: 'v3', value: _.extend([], geometry._explodeprepared.explodeAttributes.displacement.value)},
				customColor: {type: 'c', value: _.extend([], geometry._explodeprepared.explodeAttributes.customColor.value)}
			};

			this._explodeUniforms = {
				time: {type: 'f', value: 0}
			};

        }
		return new THREE.Mesh(geometry.clone(), new THREE.ShaderMaterial({
			uniforms: this._explodeUniforms,
			attributes: this._explodeAttributes,
			vertexShader: this.conf.vertexshader,
			fragmentShader: this.conf.fragmentshader,
			shading: THREE.FlatShading,
			side: THREE.DoubleSide,
			opacity: this.conf.opacity,
			transparent: true
		}));
	};

	p._addExplodeFireEffect = function(callback) {
		
		var self = this,
			texture = m.explodeTextures[this.conf.explodeTexture];


		if(! texture) {

			THREE.ImageUtils.loadTexture(this.conf.explodeTexture, undefined, function (texture) {
				m.explodeTextures[self.conf.explodeTexture] = texture;
				self._addExplodeFireEffect(callback);
			});
		
			return this;
		}

		var cloud = new THREE.Object3D(),
			scale = this.getScale();

		this.mesh.add(cloud);

		this.fire = new SpriteParticleSystem({
			particlesMoveWithEmitter: false,
			cloud: cloud,
			rate: 33,
			num: 50,
			texture: texture,
			scaleR: [.02 * scale, .07 * scale],
			speedR: [0, 2],
			rspeedR: [4, 12],
			lifespanR: [1, 2],
			terminalSpeed: 100,
			color: self.conf.color
		});

		this.fire.yScaleBase *= .5;
		this.fire.xScaleBase *= .78;

		this.fireForce = new THREE.Vector3(0, 5, 0);
		this.fire.addForce(self.fireForce);
		
		
		var center = this.center;
		//cloud.translateZ(mid.z);
		cloud.translateY(center.y + .1);
	//	cloud.translateX(center.x);

		if(callback) callback();
		
		return this;

	};

	p.resurrect = function() {
		
		this.exploding = false;

		if(this.fire)
			this._addExplodeFireEffect();
		else if(this._explodeUniforms.time)
			this._explodeUniforms.time.value = 0;

		return parent
				.resurrect.apply(this, arguments)
				.updateMaterial();
	};

	p.explode = function(explodeTime) {
		
		if(! this.exploding) {
			
			this.exploding = true;

			var self = this;
			
			if(this.fire)
				this.fire.start();
			
			new TWEEN.Tween( { x: 100} )
				.to( { x: 0 }, explodeTime || 280 )
				.easing( TWEEN.Easing.Sinusoidal.Out )
				.onUpdate( function () {
					self._updateExplodeAtts(this);
				})
				.onComplete(function () {
					if(self.fire) {
						self.fire.stop();
						self.mesh.remove(self.fire.cloud);
					}
					self.remove();
					self.onExploded(self);
				})
				.start();
		}

		return this;
	};


	p._updateExplodeAtts = function(atts) {
		var material = this.mesh.material && this.mesh.material.materials instanceof Array ? this.mesh.material.materials[0] : this.mesh.material;
		switch(this.conf.collisionEffect) {
		case 'explode':
			//material.opacity = atts.x / 100;
			this._explodeUniforms.time.value = (100 - atts.x) / 100;
			break;
		case 'bad':
			material.opacity = atts.x / 110;
			material.emissive.set(0xdd0000);
		break;
		case 'fire':
		default:
			material.opacity = atts.x / 110;
			break;
		}
		return this;

		
	};

	p.update = function(now, dt) {
		if(this.removed)
			return;

		parent.update.apply(this, arguments);
		
		if(this.fire)
			this.fire.psUpdate();
	};

	p._showHelpers = function() {
		var up = new THREE.Vector3(0, 1, 0),
			dir = new THREE.Vector3(0, 0, 1),
			right = new THREE.Vector3().crossVectors(dir, up);
		var center = new THREE.Vector3(0, 0, 0);
		this.mesh.add(new THREE.ArrowHelper(up, center, 2, 0xffffff));
		this.mesh.add(new THREE.ArrowHelper(dir, center, 2, 0xffffff));
		this.mesh.add(new THREE.ArrowHelper(right, center, 2, 0xffffff));
	};

	p.onExploded = function(packet) {
		console.warn(this, 'onExploded() not handeled.');
	};


})();