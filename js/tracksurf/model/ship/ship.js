
var TS = TS || {};

if(! TS.models) TS.models = {};

(function() {

	var mparent = TS.model,
		parent = mparent.prototype;

	TS.models.Ship = function(gameLoop, conf) {
		mparent.apply(this, arguments);
		this.trackPos = 1; // middle by default
		this.steering = null;
		this.trackOffset = 0;
		this.maxSteeringAngle(conf.steeringMaxAngle)
			.steerSensibility(conf.steerSensibility);
		if(TS.debug)
			window.ship = this;
	};


	TS.models.Ship.prototype = Object.create(parent);

	var m = TS.models.Ship,
		p = m.prototype;

	p.load = function(path, scene, position, callback) {
		var self = this;

		return parent.load.call(this, path, scene, position, function () {
			
			self.mesh.up = new THREE.Vector3(0, 1, 0);

			self.calculateCenter();

			if(TS.debug)
				self._showHelpers();
			

			self._addEngineFire(callback);
		});
	};
	
	p.steerSensibility = function(sensibility) {
		if(sensibility !== undefined) {
			this._steerSensibility = sensibility;
			return this;
		}
		return this._steerSensibility;
	};

	p.maxSteeringAngle = function(angle) {
		if(angle !== undefined) {
			this._maxSteeringAngle = angle;
			return this;
		}
		return this._maxSteeringAngle;
	};

	p._addEngineFire = function(callback) {
		var self = this;

		THREE.ImageUtils.loadTexture(this.conf.engineFireTexture, undefined, function (texture) {

			var cloud = new THREE.Object3D(),
				scale = self.getScale();

			self.mesh.add(cloud);
			
			self.fire = new SpriteParticleSystem({
				particlesMoveWithEmitter: false,
				cloud: cloud,
				rate: 13,
				num: 20,
				texture: texture,
				scaleR: [.0078 * scale, .0034 * scale],
				speedR: [0, 0],
				rspeedR: [0, 0],
				lifespanR: [1, 2],
				terminalSpeed: 10,
				color: self.conf.engineFireColor
			});

			self.fire.yScaleBase *= .5;
			self.fire.xScaleBase *= .78;

			self.fireForce = new THREE.Vector3(0, -.4, -1.2);
			self.fire.addForce(self.fireForce);
			self.fire.start();
			
			// move it to the ship's ass
			var mid = self.mesh.geometry.boundingBox.size().multiplyScalar(.5);
			cloud.translateZ(-mid.z - .3);
			cloud.translateY(-mid.y / 2 - .2);

			if(callback) callback();

		});
		
		return this;

	};

	p.update = function(now, dt) {
		parent.update.apply(this, arguments);
		if(this.fire)
			this.fire.psUpdate();

		if(this.steering){
			this.mesh.rotateZ(this.steering.rotation);
		}
	};

	p._showHelpers = function() {
		var up = new THREE.Vector3(0, 1, 0),
			dir = new THREE.Vector3(0, 0, 1),
			right = new THREE.Vector3().crossVectors(dir, up);
		var center = new THREE.Vector3(0, 0, 0);
		this.mesh.add(new THREE.ArrowHelper(up, center, 3, 0xffffff));
		this.mesh.add(new THREE.ArrowHelper(dir, center, 3, 0xffffff));
		this.mesh.add(new THREE.ArrowHelper(right, center, 3, 0xffffff));
	};

	p.setTrackParams = function(trackWidth, strips) {
		this._stripsCount = strips;
		this._widthOffset = this.size.x * this.getScale();
		this._trackLeftLimit =  -trackWidth + this._widthOffset;
		this._trackRightLimit = -this._trackLeftLimit;
		this._trackWidth = this._trackRightLimit - this._trackLeftLimit;
		this._stripWidth = this._trackWidth / strips;

		this.trackCentres = [0];
		for(var ii = 1; ii < strips - 1; ii++) {
			var off = this._widthOffset / 2 + ii * this._stripWidth;
			this.trackCentres.push(off, -off);
		}
			
		return this;1
	};

	p.setToCenterTrack = function() {
		this.trackOffset = 0;
		this.steering = null;
		return this;
	};

	p.steerLeft = function() {
		if(! this.canSteerLeft())
			return this;

		this.setSteerRotation('left');
		this.trackOffset -= this.steerSensibility() || 0.5;

		return this;
	};

	p.steerRight = function() {
		if(! this.canSteerRight())
			return this;

		this.setSteerRotation('right');
		this.trackOffset += this.steerSensibility() || 0.5;

		return this;
	};

	p.canSteerLeft = function() {
		return this.trackOffset > this._trackLeftLimit;
	};

	p.canSteerRight = function() {
		return this.trackOffset < this._trackRightLimit;
	};

	p.unsteer = function() {
		
		if(this.steering && ! this.steering.unstearing) {
			this.steering.tween.stop();

			// rotate back
			// and find center the ship to the nearest nearest strip centre
			var self = this,
				steering = {
					rotation: this.steering.rotation,
					unstearing: true
				},
				steerTime = this.conf.steerTime || 800,
				strips = this.strips,
				stripWidth = this._stripWidth,
				centres = this.trackCentres,
				nearestCentre = null;
			
			centres.map(function (centre) {
				var noff = Math.abs(self.trackOffset - centre),
					coff = Math.abs(self.trackOffset - nearestCentre);

				if(nearestCentre === null)
					nearestCentre = centre
				else if(noff < coff)
					nearestCentre = centre;
				
			});

			steering.tween = new TWEEN.Tween( { x: THREE.Math.radToDeg(steering.rotation), d: this.trackOffset } )
				.to( { x: 0, d: nearestCentre }, steerTime * 1.5 )
				.easing( TWEEN.Easing.Back.Out )
				.onUpdate( function () {
										
					self.trackOffset = this.d;
					steering.rotation = THREE.Math.degToRad(this.x);
					
				})
				.onComplete( function () {

					self.steering = null;

				})
				.start();

			this.steering = steering;

		}

		return this;
	};

	p.setSteerRotation = function(direction) {
		var steering = {
		 		rotation: 0,
		 		tween: null,
		 		direction: direction
	 		},
	 		from = this.steering ? this.steering : 0,
	 		angle = this.maxSteeringAngle() || 30,
	 		currentSteer = this.steering ? this.steering.direction : null,
	 		steerTime = this.conf.steerTime || 800;

	 	// already handled
	 	if(currentSteer == direction)
	 		return this;

	 	if(this.steering)
	 		this.steering.tween.stop();

		switch(direction) {
		case 'left':
			angle *= -1;
			break;
		case 'right':
			break;
		}

		steering.tween = new TWEEN.Tween( { x: from } )
			.to( { x: angle }, steerTime * .75 )
			.easing( TWEEN.Easing.Circular.Out )
			.onUpdate( function () {

				steering.rotation = THREE.Math.degToRad(this.x);

			})
			.start();

		this.steering = steering;

		return this;
	};



	
})();