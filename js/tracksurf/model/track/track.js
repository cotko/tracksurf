
var TS = TS || {};

if(! TS.models) TS.models = {};


(function() {

	var mparent = TS.model,
		parent = mparent.prototype;

	

	TS.models.Track = function(gameLoop, conf) {
		mparent.apply(this, arguments);
		conf = conf || {};

		this.conf = conf;

		this.setTrackTime(conf.trackTime || 35);
		this.tubeRadius = conf.tubeRadius || 3;
		this.tubeRadiusHalf = this.tubeRadius / 2;
		this.packets = {};

		// helpers for calculating the point position
		this.binormal = new THREE.Vector3(0, 0, 0);
		this.normal = new THREE.Vector3(0, 0, 0);
		
		if(TS.debug)
			window.track = this;

		this.paused(true);
	};


	TS.models.Track.prototype = Object.create(parent);

	var m = TS.models.Track,
		p = m.prototype;

		
	p.load = function(data, scene, position, callback) {
		var self = this;
		THREE.ImageUtils.loadTexture(this.conf.texture, {}, function(texture) {
			self._load(data, scene, position, callback, texture);
		});

		return this;
	};

	p.prepare = function(callback) {
		this
			.potisionShipToStart()
			.prepareTrackCameraAnimationPath()
			.ship
				.setToCenterTrack();

		if(callback) callback();

		return this;
	};

	p.start = function(countDownSecs, cbEnd) {
		
		this.cbEnd = cbEnd;

		this.prepare();

		this.initTrackCamAnimation(countDownSecs, function () {
			
			this.gameLoop.resetTime();
			this.paused(false);

		}.bind(this));
		return this;
	};

	p.setTrackTime = function(trackTime) {
		this.trackTime = trackTime;
		return this;
	};

	p.resetGame = function() {
		var packets = {};
		// not exploded ones
		for(var pp in this.packets) {
			var packet = this.packets[pp];
			if(! packets[packet.userData.type])
				packets[packet.userData.type] = [];
			packets[packet.userData.type].push(packet);
			packet.remove();
		}
		// collect exploded
		for(var pp in this.userData.explodedPackets) {
			for(var ii in this.userData.explodedPackets[pp]) {
				var packet = this.userData.explodedPackets[pp][ii];
				
				packet.resurrect();

				if(! packets[packet.userData.type])
					packets[packet.userData.type] = [];

				packets[packet.userData.type].push(packet);

			}
		}

		this.positionThePackets(packets);

		return this;
	};

	p.retryGame = function() {
		for(var pp in this.userData.explodedPackets) {
			for(var ii in this.userData.explodedPackets[pp]) {
				var packet = this.userData.explodedPackets[pp][ii];
				this.packets[packet.userData.idx] = packet;
				
				packet.resurrect();
				this.mesh.add(packet.mesh);
				packet.scene = this.mesh;

			}
		}
		return this;
	};


	p.initTrackCamAnimation = function(countDownSecs, callback) {

		var self = this;
			ship = this.ship,
			lookAt = ship.mesh.position,
			trackCamAnimtube = this.trackCamAnimtube,
			HUD = this.gameLoop.HUD(),
			duration = this.conf.trackCamAnimDuration || 15000,
			trackCamera = this.gameLoop.getCamera('track'),
			startCountdownAt = .5 + (duration - countDownSecs * 1000) / 1000,
			countdownStarted = false;

		new TWEEN.Tween( { x: 0, a: 0, cdown: 0 } )
			.to( { x: .9999, a: 1, cdown: (duration / 1000) | 0 }, duration )
			.easing( TWEEN.Easing.Quadratic.Out )
			.onUpdate( function () {
				
				trackCamera.position = trackCamAnimtube.path.getPointAt(this.x);

				trackCamera.lookAt(lookAt);

				// UP vector must be gradualy set to z as the ship is surfing on that border
				trackCamera.up.y = 1 - this.a;
				trackCamera.up.z = -this.a;

				// init countdown?
				if(! countdownStarted && this.cdown >= startCountdownAt) {
					countdownStarted = true;

					HUD.initCountdown(countDownSecs, callback);
				}

			})
			.onComplete( function () {
				
				trackCamera.lookAt(lookAt);

			})
			.start();

		return this;

	};

	p.loadShip = function(conf, callback) {
		var self = this;

		
		this.ship = new TS.models.Ship(this.gameLoop, conf).load(conf.texture, this.mesh, new THREE.Vector3(0, 0, -5), function () {
			// temp.. TODO
			self.ship.mesh.material.materials[0].emissive.r = 1;
			self.ship.mesh.material.materials[0].emissive.g = 1;
			self.ship.mesh.material.materials[0].emissive.b = 1;

			self.ship.mesh.castShadow = true;


			var flashlight = new THREE.SpotLight(0xFF8000, 50, 30, Math.PI / 10, 200); // hex, intensity, distance, angle, exponent
			self.ship.mesh.add(flashlight);
			
			if(TS.debug)
				flashlight.add(new THREE.SpotLightHelper(flashlight, 50));	
			
			flashlight.translateY(45);
			flashlight.translateZ(-85);
			flashlight.rotation.x = THREE.Math.degToRad(-30);
			flashlight.rotation.z = THREE.Math.degToRad(-10);
			flashlight.target = self.ship.mesh;

			self.setupTrackCamera();

			// set the distance for moving between tracks while steering
			self.ship
				.setTrackParams(self.tubeRadius, 3);

			if(callback) callback();
		});
		

		return this;
	};

	p.loadPackets = function(conf, callback) {
		var self = this,
			packets = {
				bad: null,
				explodable: null,
				fire: null
			};

		
		var packetsLoaded = _.after(3, function () {
			
			self.positionThePackets(packets);

			if(callback) callback();

		});

		TS.PacketFactory
			.instantiateFirePackets(this.gameLoop, conf, this.conf.packetsFire, function (pkgs) {
				packets.fire = pkgs;
				packetsLoaded();
			})
			.instantiateExplodePackets(this.gameLoop, conf, this.conf.packetsExplodable, function (pkgs) {
				packets.explodable = pkgs;
				packetsLoaded();
			})
			.instantiateBadPackets(this.gameLoop, conf, this.conf.packetsBad, function (pkgs) {
				packets.bad = pkgs;
				packetsLoaded();
			});


		return this;
	};

	p.positionThePackets = function(packets) {
		
		var self = this,
			cnt = _.reduce(packets, function(memo, p){ return memo + p.length; }, 0),
			scale = this.getScale();

		this.packets = {};

		this.userData.explodedPackets = {};
		for(var ii in packets)
			this.userData.explodedPackets[ii] = [];

		function onPacketExploded(packet) {
			
			self.userData.explodedPackets[packet.userData.type].push(packet);

			delete(self.packets[packet.userData.idx]);

		}
		

		var firstPacketOffset = this.conf.firstPacketOffset || 0,
			lastPacketOffset = this.conf.lastPacketOffset || 0,
			pathLength = this.tube.path.getLength() - firstPacketOffset - lastPacketOffset,
			step = pathLength / cnt,
			idx = 0;
				
		// tmp todo: algorithm for setting the packets on track
		for(var ii = 0; ii < cnt; ii++) {
		
			var ppos = (idx * step + (ii == 0 ? firstPacketOffset : (ii - 1 == cnt ? -lastPacketOffset : 0))) / pathLength,
				pointData = this.getPointConf(ppos),
				pos = pointData.position,
				
				
				// bad distribution
				// availablePacketsData = _.reduce(packets, function(memo, p, key){ if(p.length) { memo.packets[key] = p; memo.cnt++; } return memo; }, {packets: {}, cnt: 0}),
				// availablePacketTypes = availablePacketsData.cnt,
				// chooseFrom = availablePacketsData.packets,
				// randomPacket = Math.floor(Math.random() * availablePacketTypes) + 1,
				// randomPacketKey = Object.keys(chooseFrom)[randomPacket - 1],
				

				// should be a better distribution
				allPacketTypesLeft = _.reduce(packets, function(memo, p, key){ return memo += p.length; }, 0),
				rand = _.random(1, allPacketTypesLeft),
				sortedByCnt = Object.keys(packets).filter(function (key) { return !! packets[key].length; } ).sort(function (a, b) { return packets[a].length > packets[b].length; }),
				randomPacketKey = _.reduce(sortedByCnt, function(memo, key){ var len = packets[key].length; if(rand <= len + memo.from && rand >= memo.from) { memo.key = key; } memo.from += len; return memo; }, {key: null, from: 0}).key,
				
				packet = packets[randomPacketKey].splice(0, 1)[0];


			// delete this key as its packets holder is empy
			if(! packets[randomPacketKey].length)
				delete(packets[randomPacketKey]);
			
			// random left / right / middle track
			var trackPos = Math.round(Math.random() * 2);
			if(trackPos != 1) {
				var dir = trackPos < 1 ? -1 : 1;
				var boffset = pointData.binormal.clone().normalize().multiplyScalar(dir * (this.tubeRadiusHalf + packet.getScale() * packet.center.x));
				pos = new THREE.Vector3().subVectors(pointData.position, boffset);
			}

			packet.mesh.position = pos;

			packet.userData.trackPos = 2 - trackPos; // invert it..
			packet.userData.idx = ii;
			packet.userData.posX = pos.x;
			packet.userData.posY = pos.y;
			packet.userData.posZ = pos.z;
			packet.userData.type = randomPacketKey;
			packet.onExploded = onPacketExploded;
			

			// oriante the packet
			var lookAt = pointData.point.multiplyScalar(scale);
			lookAt.copy(pos).sub(pointData.direction);
			packet.mesh.matrix.lookAt(packet.mesh.position, lookAt, pointData.normal);
			packet.mesh.rotation.setFromRotationMatrix(packet.mesh.matrix);


			this.mesh.add(packet.mesh);
			packet.scene = this.mesh;

			this.packets[idx++] = packet;
			
		}

		return this;

	};

	p.potisionShipToStart = function(offset) {

		var pointData = this.getPointConf(this.conf.shipStartOffset || 0),
			pos = pointData.position;
				
		// position the ship to the track's start
		this.ship.mesh.position = pos;

		// orientate it 	
		var lookAt = pointData.point.multiplyScalar(this.getScale());
		lookAt.copy(pos).sub(pointData.direction);
		this.ship.mesh.matrix.lookAt(this.ship.mesh.position, lookAt, pointData.normal);
		this.ship.mesh.rotation.setFromRotationMatrix(this.ship.mesh.matrix);

		this._shipPositionWasReset = true;

		return this;
	};

	p.prepareTrackCameraAnimationPath = function() {

		// points for the animation, start at tube end - some offset
		var pointDataStart = this.getPointConf(0),
			trackCamera = this.gameLoop.getCamera('track'),
			v1 = this.tube.path.getPointAt(1),
			v2 = new THREE.Vector3(v1.x, v1.y, v1.z),
			v3 = pointDataStart.position.clone(),
			v4 = v3.clone();

		// correct some positions
		v1.x += this.conf.trackCamAnimOffsetX || 0;
		v1.y += this.conf.trackCamAnimOffsetY || 0;
		v1.z += this.conf.trackCamAnimOffsetZ || 0;

		v2.z -= 180;
		v2.x = 80;
		v2.y -= 150;

		v3.x -= 80;
		v3.z -= 30;
		v3.y += 20;

		v4.z -= this.ship.center.y;
		
		trackCamera.position = v1.clone();

		var extrudePath = new THREE.SplineCurve3([v1, v2, v3, v4]),
			tube = new THREE.TubeGeometry(extrudePath, (this.tube.path.getLength() / 2) | 0, .5, 8),
			tubeMesh = THREE.SceneUtils.createMultiMaterialObject(tube, [
				 new THREE.MeshLambertMaterial({
					opacity: TS.debug ? .2 : .05,
					transparent: true,
					wireframe: true,
					emissive: 0xb55a19
				})
			]);

		this.trackCamAnimtube = tube;

		this.mesh.add(tubeMesh);

		return this;
	};

	p.getTrackCurve = function() {
		return new THREE.SplineCurve3([
			new THREE.Vector3(0, 10, -10), new THREE.Vector3(10, 0, -10), new THREE.Vector3(20, 0, 0), 
			new THREE.Vector3(30, 0, 10), new THREE.Vector3(30, 0, 20), new THREE.Vector3(20, 0, 30), 
			new THREE.Vector3(10, 0, 30), new THREE.Vector3(0, 0, 30), new THREE.Vector3(-10, 10, 30), 
			new THREE.Vector3(-10, 20, 30), new THREE.Vector3(0, 30, 30), new THREE.Vector3(10, 30, 30), 
			new THREE.Vector3(20, 30, 15), new THREE.Vector3(10, 30, 10), new THREE.Vector3(0, 30, 10), 
			new THREE.Vector3(-10, 20, 10), new THREE.Vector3(-10, 10, 10), new THREE.Vector3(0, 0, 10),
			new THREE.Vector3(10, -10, 10), new THREE.Vector3(20, -15, 10), new THREE.Vector3(30, -15, 10),
			new THREE.Vector3(40, -15, 10), new THREE.Vector3(50, -15, 10), new THREE.Vector3(60, 0, 10),
			new THREE.Vector3(70, 0, 0), new THREE.Vector3(80, 0, 0), new THREE.Vector3(90, 0, 0),
			new THREE.Vector3(130, 5, 0), new THREE.Vector3(150, 5, 0), new THREE.Vector3(160, 20, -10),
			new THREE.Vector3(170, 20, 0), new THREE.Vector3(180, -5, 0), new THREE.Vector3(190, -15, -5),
			new THREE.Vector3(200, -15, -15), new THREE.Vector3(220, -15, -15), new THREE.Vector3(230, -15, 10),
			new THREE.Vector3(250, -20, -15), new THREE.Vector3(260, -30, -15), new THREE.Vector3(270, -20, -50),
			new THREE.Vector3(260, -10, -50), new THREE.Vector3(250, 0, -50), new THREE.Vector3(240, 0, -50),
			new THREE.Vector3(230, 0, -50), new THREE.Vector3(210, 0, -60), new THREE.Vector3(200, 0, -70),
			new THREE.Vector3(190, 10, -80), new THREE.Vector3(150, 10, -150), new THREE.Vector3(130, 10, -180),
			new THREE.Vector3(160, 10, -190), new THREE.Vector3(160, 10, -200), new THREE.Vector3(150, 10, -210),
			new THREE.Vector3(140, 10, -210), new THREE.Vector3(130, 10, -210), new THREE.Vector3(120, 20, -210),
			new THREE.Vector3(120, 30, -210), new THREE.Vector3(120, 30, -220), new THREE.Vector3(110, 30, -220),
			new THREE.Vector3(80, 30, -220), new THREE.Vector3(50, 30, -220), new THREE.Vector3(30, 30, -220),
			new THREE.Vector3(20, 30, -220), new THREE.Vector3(10, 35, -220), new THREE.Vector3(0, 40, -220)
		]);
	};


	p.setupTrackCamera = function() {
		this.gameLoop.addCamera(
			'track',
			new THREE.PerspectiveCamera(84, window.innerWidth / window.innerHeight, 0.01, this.tube.path.getLength() + 20)
		);
		var trackCamera = this.gameLoop.getCamera('track');
		
		if(TS.debug) {
			trackCamera.cameraHelper = new THREE.CameraHelper(trackCamera);
			trackCamera.cameraHelper.scale.multiplyScalar(0.1);
			trackCamera.add(trackCamera.cameraHelper);
		}

		this.mesh.add(trackCamera);


		return this;
	};


	p._load = function(data, scene, position, callback, texture) {

		this.mesh = new THREE.Object3D();

		var pipeSpline = this.getTrackCurve();

		var segments = 850;
		var radiusSegments = 3;
		var closed2 = false;
		var debug = true;
		var extrudePath = pipeSpline;

		var tube = new THREE.TubeGeometry(extrudePath, segments, this.tubeRadius, radiusSegments, closed2, debug);

				
		texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set( 1, 1 );
		texture.minFilter = THREE.LinearFilter;
		texture.flipY = true;
		texture.mapping = THREE.SphericalRefractionMapping;
		texture.anisotropy = 0;

		
		var tubeMesh = THREE.SceneUtils.createMultiMaterialObject(tube, [
			// new THREE.MeshLambertMaterial({
			// 		color: 0xFF8528,
			// 		opacity: (tube.debug) ? 0.2 : 0.1,
			// 		transparent: true
			// }),
			//  new THREE.MeshBasicMaterial({
			// 		opacity: .1,
			// 		transparent: true,
			// 		wireframe: true,
			// 		specular: 0xff0000,

			// }),
			new THREE.MeshPhongMaterial( {
				color: 0xbbbb00,     // reflectivity for diffuse light
				ambient: 0xbbbb00,   // reflectivity for ambient light
				emissive: 0x333333,         // emissive color; this is the default (black)
				specular: 0x505050,  // reflectivity for specular light
				shininess: 50,        // controls size of specular highlights
				//wireframe: true,
				map: texture,
				transparent: true,
				opacity: .8
			})
		]);
		
		this.mesh.add(tubeMesh);
			
		if(TS.debug)
			window.tubeMesh  =tubeMesh;
		

		// wireframed tube arround the "track"
		var tube2 = new THREE.TubeGeometry(extrudePath, 200, this.tubeRadius * 2, 50, closed2);
		var tubeMesh2 = THREE.SceneUtils.createMultiMaterialObject(tube2, [
			// new THREE.MeshLambertMaterial({
			// 		color: color,
			// 		opacity: (geometry.debug) ? 0.2 : 0.1,
			// 		transparent: true
			// }),
			new THREE.MeshBasicMaterial({
				opacity: .2,
				transparent: true,
				wireframe: true,
				specular: 0xff0000,
				map: texture
			})
		]);

		this.mesh.add(tubeMesh2);


		var scale = this.getScale();
		tubeMesh.scale.set(scale, scale, scale);

		
		this.tube = tube;
		
		this._oncreated(this.mesh, scene, position, tube, callback);

		return this;
	};

	p.animateCamera = function(now, dt) {
		
		var ship = this.ship,
			tube = this.tube,
			scale = this.getScale(),
			point = this.getPointForDt(dt)
			pointData = this.getPointConf((point + (this.conf.shipStartOffset || 0)) % 1); // go to beginning of track if initial shipt offset was set

		// update position to left / right if not on center
		var pos = pointData.position;

		if(ship.trackOffset != 0) {

			var boffset = pointData.binormal.clone().normalize().multiplyScalar(-ship.trackOffset);
			pos = new THREE.Vector3().subVectors(pointData.position, boffset);
		

		}
		// console.log(pointData.position, pos)

		ship.mesh.position = pos;

		ship.userData.posX = pos.x;
		ship.userData.posY = pos.y;
		ship.userData.posZ = pos.z;
		

		var lookAt = pointData.position.clone().multiplyScalar(scale);
		//var lookAt = new THREE.Vector3();
		lookAt.copy(pos).sub(pointData.direction);
		ship.mesh.matrix.lookAt(ship.mesh.position, lookAt, pointData.normal);
		ship.mesh.rotation.setFromRotationMatrix(ship.mesh.matrix);


		
		var soffset = pointData.direction.clone().normalize().multiplyScalar(3);
		var soffset2 = pointData.normal.clone().normalize().multiplyScalar(-1);
		var spos = new THREE.Vector3().subVectors(pointData.position, soffset).sub(soffset2);
		
		var trackCamera = this.gameLoop.getCamera('track');
		trackCamera.position = spos.clone();
		
		if(ship.trackOffset != 0) {
			
			trackCamera.translateX(ship.trackOffset);
		}

		// if(ship.trackPos != 1) {
		// 	var dir = ship.trackPos == 0 ? -1 : 1;
		// 	trackCamera.translateX(dir * this.tubeRadiusHalf)
		// }

		//trackCamera.translateOnAxis(new THREE.Vector3(0, 0, 2), 1.2);
		trackCamera.matrix.lookAt(trackCamera.position, lookAt, pointData.normal);
		trackCamera.rotation.setFromRotationMatrix	(trackCamera.matrix);

		return;

	};


	p.checkForCollisions = function() {
		var ship = this.ship,
			precision = ship.userData.precision;
		
		if(! precision) {
			precision = {
				x: .8,
				y: .8,
				z: .8,
			}
			ship.userData.precision = precision;	
		}

		for(var pp in this.packets) {
			var packet = this.packets[pp];

			var diffx = Math.abs(ship.userData.posX - packet.userData.posX),
				diffy = Math.abs(ship.userData.posY - packet.userData.posY),
				diffz = Math.abs(ship.userData.posZ - packet.userData.posZ);

			
			if(diffx < precision.x && diffy < precision.y && diffz < precision.z) {
				

				if(! packet.exploding) {
					
					packet.explode();
					this.onCollision(packet.userData);
				}

				break;
			}

		}

		return this;
	};



	p.getPointForDt = function(dt) {
		return (dt % this.trackTime) / this.trackTime;
	};
	
	/**
	 * @param "double" point: point on the track, between 0 and 1
	 */
	p.getPointConf = function(pointPos) {
		var tube = this.tube,
			normal = this.normal,
			binormal = this.binormal;
		// get the point
		var pos = tube.path.getPointAt(pointPos),
			point = pos.clone();
		// consider scaling
		pos.multiplyScalar(this.getScale());
		// interpolate the point
		var segments = tube.tangents.length,
			pickt = pointPos * segments,
			pick = Math.floor(pickt),
			pickNext = (pick + 1) % segments,
			direction = tube.path.getTangentAt(pointPos);

		// tube's tangent is pointed into tube's "direction"
		// normal is pointed from tangent "up"
		// binormal is pointed to the "right"; cross between tangent and normal

		binormal.subVectors(tube.binormals[pickNext], tube.binormals[pick]);
		binormal.multiplyScalar(pickt - pick).add(tube.binormals[pick]);
		

		// binormal is now interpolated between requested position and closest defined position on the tube
		// copy it and cross it with the tangent, to get the normal for requested position
		normal.copy(binormal).cross(direction).clone();

		// move on a offset on its binormal
		pos.add(normal.clone().multiplyScalar(1.8));

		return {
			normal: normal,
			binormal: binormal,
			direction: direction,
			position: pos,
			point: point
		};
	};

	p.paused = function(paused) {
		if(paused !== undefined) {
			this._paused = !! paused;
			return this;
		}
		return this._paused;
	};

	p.update = function(now, dt) {

		if(this.paused())
			return this;

		this.animateCamera(now, dt);

		parent.update.apply(this, arguments);
		this.ship.update(now, dt);
		for(var pp in this.packets)
			this.packets[pp].update(now, dt);
		
		this.checkForCollisions();
		
		if(dt > this.trackTime)
			if(this.cbEnd)
				this.cbEnd();

		return this;
	};

	p.onCollision = function() {
		console.log(this, 'onCollision() not overridden.');
	};

})();


