
var TS = TS || {};

if(! TS.models) TS.models = {};


(function () {

	var loader = new THREE.JSONLoader();

	
	TS.model = function(gameLoop, conf) {
		this.conf = conf || {};
		this._uuid = THREE.Math.generateUUID();
		this.gameLoop = gameLoop;
		this.removed = false;
		this.userData = {};
	};

	TS.model.prototype = {
		get mesh() {
			return this._mesh;
		},

		set mesh(mesh) {
			this._mesh = mesh;
		},

		get uuid() {
			return this._uuid;
		}
	};
	

	var m = TS.model,
		p = m.prototype;

	m.Loader = loader;

	p.load = function(path, scene, position, callback) {
		var self = this;
		//console.log('loading:', path);
		loader.load(path, function (geometry, mats) {
			
			self._oncreated(mats, scene, position, geometry, callback);
		});
		return this;
	};

	p._createMesh = function(mats, geometry) {
		return new THREE.Mesh(geometry, new THREE.MeshFaceMaterial(mats));
	};

	p._oncreated = function(mats, scene, position, geometry, callback) {

		if(! this.mesh)
			this.mesh = this._createMesh(mats, geometry);
		
		
		if(! geometry.boundingBox)
			geometry.computeBoundingBox();
		

		var scale = this.getScale();
		this.mesh.scale.set(scale, scale, scale);

		if(position)
			this.mesh.position = position;

		if(scene) {
			scene.add(this.mesh);
			this.scene = scene;
		}
		
		

		if(callback) callback();
	};

	p.remove = function() {
		if(! this.scene) {
			console.log(this, 'remove(): no scene found to remove from.');
			return;
		}
		this.scene.remove(this.mesh);
		this.removed = true;
		return this;
	};

	p.resurrect = function() {
		this.removed = false;
		return this;
	};

	p.update = function(now, dt) {
		if(this.removed)
			return this;
	};
	
	p.getScale = function() {
		return this.conf.scale === undefined ? 1 : this.conf.scale;
	};

	p.calculateCenter = function() {
		if(! this.mesh.geometry.boundingBox)
			this.mesh.geometry.computeBoundingBox();
		var boundingBox = this.mesh.geometry.boundingBox,
			center = new THREE.Vector3();
		center.subVectors(boundingBox.max, boundingBox.min);
		center.multiplyScalar(0.5);
		//center.add(boundingBox.min);
		this.center = center;
		this.size = boundingBox.size();
		return this;
	};

})();