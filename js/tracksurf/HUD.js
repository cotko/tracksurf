
var TS = TS || {};

(function () {

	function countDown(callback, completecb) {
		new TWEEN.Tween( { x: 1} )
			.to( { x: 0 }, 1000 )
			.easing( TWEEN.Easing.Quadratic.In )
			.onUpdate( function () {
				
				callback(this.x);

			})
			.onComplete(function () {
				completecb();
			})
			.start();
	};

	TS.HUD = function(conf) {
		
		this.conf = conf || {};

		var cnv = document.createElement('canvas'),
			ctx = cnv.getContext('2d');

		cnv.style.position = 'absolute';
		cnv.style.display = 'block';
		cnv.style.top = '0';
		cnv.style.left = '0';
		cnv.style.zIndex = 3;

		this.conf.container.appendChild(cnv);

		cnv.width = conf.width;
		cnv.height = conf.height;
				
		ctx.textAlign = 'center';
		
		this.canvas = cnv;
		this.ctx = ctx;

		this.widthHalf = this.conf.width / 2;

	};

	var m = TS.HUD,
		p = TS.HUD.prototype;


	p.initCountdown = function(secs, callback) {
		var textPx = this.conf.countdownSize || 150,
			font = this.conf.countDownFont || 'Arial',
			goWord = this.conf.goWord || 'GO',
			ctx = this.ctx,
			widthHalf = this.widthHalf,
			opacity = .9,
			rgb = (this.conf.coutndounwRGB || [255, 255, 255]).join(', ');

		ctx.font = textPx + 'px ' + font;
		ctx.fillStyle = 'rgba('+ rgb +', '+ opacity +')';

		var tw = ctx.measureText(secs).width,
			th = textPx * 1.5;

		function drawCountdown() {

			countDown(function (x) {
				
				if(x < opacity) {
					ctx.clearRect(widthHalf - tw / 2, 40, tw, th);	
					ctx.fillStyle = 'rgba('+ rgb +', ' + (Math.round(x * 100) / 100) + ')';
					ctx.fillText(secs, widthHalf, textPx);
				}
				
			}, function () {
				if(--secs >= 0) {
					ctx.clearRect(widthHalf - tw / 2, 40, tw, th);
					ctx.fillStyle = 'rgba('+ rgb +', '+ opacity +')';
					ctx.fillText(secs, widthHalf, textPx);
					drawCountdown();
				} else {
					ctx.fillStyle = 'rgba('+ rgb +', .5)';
					ctx.fillText(goWord, widthHalf, textPx);
					setTimeout(function () {
						var goWidth = ctx.measureText(goWord).width;
						ctx.clearRect(widthHalf - goWidth / 2, 40, goWidth, th);
						callback();
					}, 250);
				}
			});

		};

		ctx.fillText(secs, widthHalf, textPx);

		drawCountdown();

		return this;
	};

	p.clearPoints = function() {
		var lastPointsData = this.lastPointsData;
		if(lastPointsData)
			this.ctx.clearRect(lastPointsData.txOff - lastPointsData.tw / 2, lastPointsData.tyOff - lastPointsData.th, lastPointsData.tw, lastPointsData.th + 20);
		return this;
	};

	p.setPoints = function(points) {

		if(! this._setPoints) {
			
			var self = this;

			this._setPoints = (function() {
				
				var rgb = (self.conf.pointsRGB || [255, 255, 225]).join(', '),
					textPx = self.conf.pointsSize || 150,
					font = self.conf.pointsFont || 'Arial',
					ctx = self.ctx,
					widthHalf = self.widthHalf,
					opacity = .9,
					th = textPx * 1.5,
					tyOff = self.conf.height - th / 8;

				self.lastPointsData = {};

				return function(points) {
					
					self.clearPoints();

					ctx.font = textPx + 'px ' + font;
					ctx.fillStyle = 'rgba('+ rgb +', '+ opacity +')';

					var tw = ctx.measureText(points).width,
						txOff = self.conf.width - 8 - tw / 2;

					ctx.fillText(points, txOff, tyOff);

					self.lastPointsData.tw = tw;
					self.lastPointsData.th = th;
					self.lastPointsData.txOff = txOff;
					self.lastPointsData.tyOff = tyOff;

				};

			})();

		}

		this._setPoints(points);

		return this;
	};

	p.addPoints = function(from, howMany) {

		if(this._lastPointAnimTween)
			this._lastPointAnimTween.stop();

		var self = this;

		this._lastPointAnimTween = new TWEEN.Tween( { x: from} )
			.to( { x: from + howMany }, this.conf.pointAnimationTime )
			.easing( TWEEN.Easing.Quadratic.Out )
			.onUpdate( function () {
				
				self.setPoints(Math.round(this.x));
				
			})
			.start();

		return this;
	};




})();
