
var TS = TS || {};

(function () {

	TS.GameLoop = function(conf, callback, onLoad) {
		
		if(typeof(onLoad) == 'function')
			this.onLoad = onLoad;

		this.conf = conf || {};
		this.container = this.conf.container || document.body;
		this.width = conf.width || this.container.innerWidth || this.container.offsetWidth;
		this.height = conf.height || this.container.innerHeight || this.container.offsetHeight;
		this.models = {};
		this.cameras = {};
		this.userData = {};
		this.prepare(callback);
		TS.KeyboardHandler.init();
	};

	var m = TS.GameLoop,
		p = m.prototype;


	p.prepare = function(callback) {
		return this
					.initHUD()
					.initRenderer()
					.initScene()
					.initModels(callback);
	};

	p.run = function() {
		
		var self = this,
			now = null,
			dt = null;

		this.resetTime();


		(function runner() {

			now = new Date().getTime() / 1000;
			
			dt = now - self.lastFrameTime;
			self.update(now, dt);
			
			self.renderer.render(self.scene, self.camera);
			
			if(self.stats) self.stats.update();
			
			//self.lastFrameTime = now;

			requestAnimationFrame(runner);

		})();
		
		return this;
	};

	p.resetTime = function() {
		this.lastFrameTime = new Date().getTime() / 1000;
		return this;
	};

	p.update = function(now, dt) {
		
		TWEEN.update(); // if there are any tweens

	//	console.log(this.models, this)
		for(var mm in this.models)
			this.models[mm].update(now, dt);

		this.handleKeyboard();

		return this;
	};


	p.initRenderer = function() {

		var renderer = new THREE.WebGLRenderer({
			antialias: false,
			clearColor: 0x000000
		});

		// renderer.physicallyBasedShading = true;
		// renderer.gammaInput = true;
		// renderer.gammaOutput = true;

		renderer.setSize( this.width, this.height );
		renderer.domElement.style.position = 'relative';
		renderer.domElement.style.display = 'block';

		this.conf.container.appendChild(renderer.domElement);	

		this.renderer = renderer;

		// stats
		if(this.conf.stats) {
			var stats = new Stats();
			stats.domElement.style.position = 'absolute';
			stats.domElement.style.top = '0px';
			this.container.appendChild(stats.domElement);
			this.stats = stats;
		}

		return this;
	};

	p.initScene = function() {
		this.addCamera('main', new THREE.PerspectiveCamera(70, this.width / this.height, 1, 10000))
			.setCamera('main');
		
		this.scene = new THREE.Scene();
		this.scene.add(this.camera);
		
		this.camera.lookAt(new THREE.Vector3());
		this.camera.position = new THREE.Vector3(0, 0, 180);
		
		return this;
	};

	p.initModels = function() {
		return this;
	};

	p.initHUD = function() {
		this._HUD = new TS.HUD(this.conf.HUD);

		if(TS.debug)
			window.HUD = this._HUD;

		return this;
	}

	p.addModel = function(model, key) {
		this.models[key || model.uuid] = model;
	};

	p.removeModel = function(model, key) {
		delete(this.models[key || model.uuid]);
	};

	p.getModel = function(key) {
		return this.models[key];
	};

	p.addCamera = function(key, camera) {
		this.cameras[key] = camera;
		camera.name = key;
		return this;
	};

	p.setCamera = function(key) {
		this.camera = this.cameras[key];
		return this;
	};

	p.getCamera = function(key) {
		return this.cameras[key];
	};

	p.handleKeyboard = function() {
		return this;
	};
	
	p.HUD = function() {
		return this._HUD;
	};

	p.onLoad = function(percent) {
		console.log(this, 'onLoad() not overridden. Done: ', percent);
	};

})();




