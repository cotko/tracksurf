
var TS = TS || {};

(function () {
	
	var inited = false,
		keysToHandle = {};


	function handleKeyDown(e) {
		keysToHandle[e.which] = e;
	}

	function handleKeyUp(e) {
		delete(keysToHandle[e.which]);
	}


	TS.KeyboardHandler = {


		init: function () {

			if(! inited) {

				inited = true;
				
				document.addEventListener("keydown", handleKeyDown, false);
				document.addEventListener("keyup", handleKeyUp, false);

			}

			return this;
		},

		isDown: function (which) {
			return keysToHandle[which];
		},

		keyCode: {
			BACKSPACE: 8,
			COMMA: 188,
			DELETE: 46,
			DOWN: 40,
			END: 35,
			ENTER: 13,
			ESCAPE: 27,
			HOME: 36,
			LEFT: 37,
			PAGE_DOWN: 34,
			PAGE_UP: 33,
			PERIOD: 190,
			RIGHT: 39,
			SPACE: 32,
			TAB: 9,
			UP: 38
		}

	};


})();