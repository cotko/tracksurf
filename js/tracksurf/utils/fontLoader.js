
var TS = TS || {};

(function () {

	TS.FontLoader = function() {

	};

	var m = TS.FontLoader,
		p = TS.FontLoader.prototype;

	m.loadFonts = function($target) {
		for(var ii = 1; ii < arguments.length; ii++) {
			$('<div />').html(ii).css({
				fontFamily: arguments[ii],
				width: 0,
				height: 0,
				fontSize: 1,
				overflow: 'hidden'
			}).appendTo($target);
		}
		return this;
	};

})();