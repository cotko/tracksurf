

var TS = TS || {};

if(! TS.media) TS.media = {};

(function () {


	var mediaLoader = function(media, callback, tick) {
		// isFinite returns false if your number is
		// POSITIVE_INFINITY, NEGATIVE_INFINITY or NaN
		if (isFinite(media.duration)) {

			// http://dev.w3.org/html5/pf-summary/video.html#the-ready-states
			if (media.readyState == 4) {
				if (callback) callback();
			}

		} else {
			setTimeout(function() {
				mediaLoader(media, callback, tick);
			}, tick || 100);
		}
	};

	var audioLoader = function(src, callback, tick) {
		var audio = new Audio(src);
		mediaLoader(audio, function() {
			if(callback)
				callback(audio);
		}, tick);
	};

	TS.media.MediaLoader = mediaLoader;
	TS.media.AudioLoader = audioLoader;

})();