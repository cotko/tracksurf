

var TS = TS || {};

(function () {
	
	var disabled = false,
		$leContainer = null;


	var interface = {

		init: function($container, onStart) {
			$leContainer = $container;
			$container
				.find('.levels > div')
					.on('click', function () {
						
						if(disabled)
							return;

						$(this)
							.siblings()
								.removeClass('selected', false)
								.end()
							.addClass('selected');
					})
					.end()
				.find('#startbutton')
					.on('click', function () {
						
						if(disabled)
							return;
						
						onStart($container.find('.levels .selected').index() + 1);
					
					});

			return interface;
		},

		show: function() {
			this.disable(false);
			$leContainer.fadeIn('fast');
			return interface;
		},

		hide: function() {
			this.disable(true);
			$leContainer.fadeOut('fast');
			return interface;
		},

		disable: function() {
			disabled = true;
			$leContainer.addClass('disabled');
			return interface;
		},

		enable: function() {
			disabled = false;
			$leContainer.removeClass('disabled');
			return interface;
		}
		
	};

	TS.Loader = interface;

})();